import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
// import './App.css'
import Keycloak from 'keycloak-js';
import Navbar from "@/scenes/navbar"
import WeekSelector from '@/scenes/weekselector';

import { Table, Input, Pagination } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { PlusCircleOutlined } from '@ant-design/icons';
import { Button, Tooltip, Select } from 'antd';


import {
  QueryClient,
  QueryClientProvider,
  useQuery,
  useMutation,
  useQueryClient
} from '@tanstack/react-query'

const queryClient = new QueryClient()

function formatDate(date = new Date()) {
  const year = date.toLocaleString('default', {year: 'numeric'});
  const month = date.toLocaleString('default', {
    month: '2-digit',
  });
  const day = date.toLocaleString('default', {day: '2-digit'});

  return [year, month, day].join('-');
}

type Project = {
  id: number;
  name: string;
}

type Task = {
  title: string;
  header?: string;
  project: Project;
  minutes: string;
  date: Date;
}

type ExistingTaskRow = {
  title: string;
  project: string;
  duration: string;
}

type WeekdayTaskRow = {
  weekday: string;
  date: string;
  hours: number;
  addTaskVisible: boolean;
}

type NewTaskRow = {
  jeszczeniewiem:boolean
  date: Date
}

type EmptyTaskRow = {
  empty: boolean;
}

type TaskRow = WeekdayTaskRow | ExistingTaskRow | EmptyTaskRow | NewTaskRow

function App() {

  var wS: Date = new Date()
  wS.setDate(wS.getDate() - wS.getDay() + 1)
  wS.setHours(0,0,0,0)


  const [count, setCount] = useState(1)
  const [weekStart, setWeekStart] = useState(wS)
  const [organization, setOrganization] = useState("1")

  useEffect(() => {
    console.log("Token " + sessionStorage.getItem('token'))
    let initOptions = {
      url: 'http://localhost:8080/auth',
      realm: 'Marek',
      clientId: 'test',
      // onLoad: 'login-required',
      // KeycloakResponseType: 'code'
    }
    

  }, [])

  const organizationsRes = useQuery({
    
    queryKey: ['organizations'],
    queryFn: () =>
      fetch('http://localhost:8080/graphql', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + sessionStorage.getItem('token')
      },
      body: JSON.stringify({query: `query getOrganizations{
        getOrganizations {
          id
          name
        }
      }`})
    }).then(
        (res) => res.json(),
      ),
  })

  return <div className="app bg-gray-20 bg-gray">

    <Navbar organization={organization} setOrganization={setOrganization} organizations={organizationsRes.data?.data?.getOrganizations} />
    <WeekSelector count={count} setCount={setCount} setWeekStart={setWeekStart} weekStart={weekStart}/>
    <Example weekStart={weekStart} organization={organization}/>
  </div>
}

type Props = {
  weekStart: Date;
  organization: string;
}

function Example(props: Props) {
  const [pageIndex, setPageIndex] = useState(1)
  const [tableData, setTableData] = useState<Task[]>([])
  const [hoveredDay, setHoveredDay] = useState<string | undefined>(undefined)
  const [newTaskDay, setNewTaskDay] = useState<string | undefined>(undefined)
  const [taskTitle, setTaskTitle] =  useState("")
  const [taskProject, setTaskProject] = useState<{
    projectId: string;
    projectName: string;
  } | undefined>(undefined)
  const [taskDuration, setTaskDuration] =  useState(0)

  const queryClient = useQueryClient()
  const weekEnd = new Date(props.weekStart)
  weekEnd.setDate(weekEnd.getDate() + 7)

  const mutation = useMutation({
    mutationFn: (newTask: any) =>
    fetch('http://localhost:8080/graphql', {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + sessionStorage.getItem('token')
    },
    body: JSON.stringify({query: `mutation addTask($title: String!, $projectId: ID!, $date: Date!, $duration: Int!) {
      addTask(title: $title, projectId: $projectId, date: $date, duration: $duration) {
        id
      }
    }`,
    variables: {"title": newTask.title, "projectId": newTask.projectId, "date": formatDate(newTask.date), "duration": newTask.duration}
  })
  }).then(
      (res) => res.json(),
    ),
    onSuccess: (data, context)  => {
      setNewTaskDay(undefined)
      const current: any = queryClient.getQueryData(['tasks', props.weekStart, props.organization])
      current.data.tasks.tasks.splice(0, 0, {
        "id": data.data.addTask.id,
        "title":context.title,
        "date":formatDate(context.date),
        "minutes": context.duration, // Conversion
        "project":{"id":context.projectId,"name":context.projectName}})
      queryClient.setQueryData(['tasks', props.weekStart], current)
    }
  })

  const tasksRes = useQuery({
    
    queryKey: ['tasks', props.weekStart, props.organization],
    queryFn: () =>
      fetch('http://localhost:8080/graphql', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + sessionStorage.getItem('token')
      },
      body: JSON.stringify({query: `query getTasks($orgId: ID, $from: Date!, $to: Date!) {
        tasks(organizationId: $orgId, from: $from, to: $to) {
          total
          tasks {
            id
            title
            date
            minutes
            project {
              id
              name
            }
          }
        }
      }`,
      variables: {"orgId": props.organization, "from": formatDate(props.weekStart), "to": formatDate(weekEnd)}
    })
    }).then(
        (res) => res.json(),
      )
  })

  const projectsRes = useQuery({
    
    queryKey: ['projects', props.organization],
    queryFn: () =>
      fetch('http://localhost:8080/graphql', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + sessionStorage.getItem('token')
      },
      body: JSON.stringify({query: `query getProjects($orgId: ID!) {
        getProjects(organizationId: $orgId) {
          total
          pageNo
          projects {
            id
            name
          }
        }
      } `,
      variables: {"orgId": props.organization}
    })
    }).then(
        (res) => res.json(),
      ),
  })
  const {data, isPending, error} = tasksRes


  // if (isPending) return 'Loading...'

  if (error) return 'An error has occurred: ' + error.message

  
  let ddd: Task[]
  if (isPending) {
    ddd = tableData
  } else {
    const total = data.data.tasks.total

    ddd = data.data.tasks.tasks.map( (t: Task) => {
      t.date = new Date(t.date)
      return t
    })
  }

  const columns2: ColumnsType<TaskRow> = [
    {
      dataIndex: 'title',
      key: 'title',

      render: (p, row) => {
        if ('empty' in row) {
          return {children: 
            <div  className={`text-center text-primary-300`}>
              <p className={`text-lg`}>No tasks for this day</p>       
            </div>, props: {
              colSpan: 3
            }
          }
        }
        if ('weekday' in row) {
          const showAddButton = hoveredDay == row.weekday ? "visible" : "invisible"

          return {children: 
            <div className= {`flex justify-center`} onMouseOver={() => {setHoveredDay(row.weekday)}} onMouseOut={() => {setHoveredDay(undefined)}}>
            <div  className={`text-center text-primary-300 bg-teal-700`}>
              <p className={`text-lg`}>{row.weekday}</p> 
              <p className={`text-sm`}>{row.date}</p> 
            </div>
            <span className={`${showAddButton} origin-left`}>
          <Tooltip title="Add">
            <Button  onClick={() => setNewTaskDay(row.weekday)} type="primary" shape="circle" icon={<PlusCircleOutlined />}  />
          </Tooltip>
          </span>                  
            </div>, props: {
              colSpan: 3
            }
          }
        }
        if ('jeszczeniewiem' in row) {
          const options:{
            value: string;
            label: string;
          }[] = projectsRes.data.data.getProjects.projects.map ((p: any) => {
            return {
              value: p.id,
              label: p.name
            }
          }
          )

          const onChange = (value: string, option: {
            value: string;
            label: string;
          } | {
            value: string;
            label: string;
          } []
          ) => {
            if (!Array.isArray(option)) {
              console.log(`selected ${value}`);
              setTaskProject({
                "projectId": option.value,
                "projectName": option.label
              })  
            }

          };
          
          const onSearch = (value: string) => {
            console.log('search:', value);
          };
          
          // Filter `option.label` match the user type `input`
          const filterOption = (input: string, option?: { label: string; value: string }) =>
            (option?.label ?? '').toLowerCase().includes(input.toLowerCase());

          return {children: 
            <div className= {`flex gap-8`}>
            <Input placeholder="Task title" onChange={ (e) => {setTaskTitle(e.target.value)}}/>
            <Select
              showSearch
              placeholder="Select a Project"
              optionFilterProp="children"
              onChange={onChange}
              onSearch={onSearch}
              filterOption={filterOption}
              options={options}
            />
            <Input placeholder="Duration" onChange={ (e) => {setTaskDuration(+e.target.value)}}/>
            <Tooltip title="Add">
              <Button  onClick={() => mutation.mutate({"date": row.date, "title": taskTitle, "duration": taskDuration, "projectId": taskProject?.projectId, "projectName": taskProject?.projectName})} type="primary" shape="circle" icon={<PlusCircleOutlined />}  />
            </Tooltip>
            </div>, props: {
              colSpan: 3
            }
          }
        }
        return {children: <div className={`app`}><p>{p}</p> </div>, props: {
          colSpan: 1
        }
      }
      }
    },
    {
      dataIndex: 'project',
      key: 'project',
      render: (p, row) => {
        if (!('title' in row)) {
          return {children: <div></div>, props: {
            colSpan: 0
          }
        }
      } else {
        return {children: <div className={`app`}><p>{p}</p> </div>, props: {
          colSpan: 1
        }
        }
      }
    }},
    {
      dataIndex: 'duration',
      key: 'duration',
      render: (p, row) => {
        if (!('title' in row)) {
          return {children: <div>x</div>, props: {
            colSpan: 0
          }
        }
      } else {
        return {children: <div className={`app`}><p>{p}</p> </div>, props: {
          colSpan: 1
        }
        }
      } 
    }
    }
  ]

  var tr: TaskRow[] = []

  var day = new Date(props.weekStart)

  for (let i = 0; i < 7; i++) {
    
    let tasks = ddd.filter((e) => { return formatDate(e.date) == formatDate(day) })
    
    tr.push({
      weekday: day.toLocaleDateString("en-US", { weekday: 'long' }),
      hours: 4,
      addTaskVisible: false,
      date: day.toLocaleDateString()
    })

    if (newTaskDay == day.toLocaleDateString("en-US", { weekday: 'long' })) {
      tr.push({
        jeszczeniewiem: true,
        date: new Date(day)
      })
    }

    tasks.forEach((e) => {
      tr.push({
        title: e.title + " ",
        date: formatDate(e.date),
        duration: e.minutes,//e.duration,
        project: e.project.name//e.project
      })
    })
    
    day.setDate(day.getDate() + 1)
  }

  return (
    <div>
    <Table rowClassName={`bg-teal-200`} pagination={false} columns={columns2} dataSource={tr}></Table>
    </div>
  )
}

export default App
