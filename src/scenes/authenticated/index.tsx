type Props = {
}
import { Button, Checkbox, Form, Input } from 'antd';
import { useNavigate } from 'react-router-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import TasksScreen from '@/scenes/tasksScreen'
import Auth from '@/scenes/auth'
import Organizations from '@/scenes/organization'
import Navbar from "@/scenes/navbar"

import {
    QueryClient,
    QueryClientProvider,
    useQuery,
    useMutation,
    useQueryClient
  } from '@tanstack/react-query'
  


import Projects from '@/scenes/projects'

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  
  type FieldType = {
    project: string;
  };
  
const Authenticated = (props: Props) => {
    

    return (<div>
        <Navbar organization={""} setOrganization={undefined} organizations={[]} />
        <Routes>
        <Route path="/" element={ <TasksScreen orgId={""}/>} />
        <Route path="/org" element={<Organizations />} />
        <Route path="/projects" element={<Projects organization={""}/>} />
      </Routes>
    </div>

    )
}

export default Authenticated