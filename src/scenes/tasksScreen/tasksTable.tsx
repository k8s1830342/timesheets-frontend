import { useGetTasks, useGetProjects, useAddTask } from "@/services/service"
import { formatDate } from "@/commons/utils"
import WeekdayHeader from "./weekdayHeader";
import { Table, Input, Pagination } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { useState } from "react";
import NewTask from "./newTask";

type Props = {
    orgId: string | undefined
    dateRange: DateRange
}

type TaskRow = {
    id: string;
    title: string;
    project: string;
    duration: string;
    date: Date;
}
  
type Task = {
    id: string;
    title: string;
    project: Project;
    minutes: string;
    date: Date;
  }
  

const TasksTable = (props: Props) => {
  const tasksRes = useGetTasks(props.orgId, props.dateRange.from, props.dateRange.to)
  const projectsRes = useGetProjects(props.orgId)
  const addTask = useAddTask()
  const [addNewTaskVisible, setAddNewTaskVisible] = useState<number|undefined>(undefined)

  if (tasksRes.data == null || projectsRes.data == null) {
    return (<p>waiting</p>)
  }

  var day = new Date(props.dateRange.from)
  const weekdays = []

  const columns: ColumnsType<TaskRow> = [
    {
      dataIndex: 'title',
      key: 'title',
    },
    {
      dataIndex: 'project',
      key: 'project',
    },
    {
      dataIndex: 'duration',
      key: 'duration'
    }
  ]
  for (let i = 0; i < 7; i++) {
    let tasks: TaskRow[] = tasksRes.data.data.tasks.tasks.map( (t: Task) => {
        return {
            id: t.id,
            title: t.title,
            project: t.project.name,
            duration: t.minutes,
            date: new Date(t.date)
        }
      }).filter((task: TaskRow) => { 
        console.log(JSON.stringify(task))
        return formatDate(task.date) == formatDate(day)
    })

    weekdays.push(
        <div key={'div'+i}>
        <WeekdayHeader key={i} index={i} setAddNewTaskVisible={setAddNewTaskVisible} name={day.toLocaleDateString("en-US", { weekday: 'long' })}/>
        <NewTask visible = {addNewTaskVisible == i} addTask = {addTask} projects = {projectsRes.data.data.getProjects.projects} date={new Date(day)} orgId = {props.orgId} weekStart={props.dateRange.from}/>
        <Table key={'table ' + i} showHeader={false} rowClassName={`bg-teal-200`} pagination={false} columns={columns} dataSource={tasks} rowKey={'id'} ></Table>
        </div>
     )

    day.setDate(day.getDate() + 1)
  }

  return (

        <div>
            {weekdays}

        </div>

  )
}

export default TasksTable