import { useState } from "react"
import { Bars3Icon, XMarkIcon} from "@heroicons/react/24/solid"
import Logo from "@/assets/Logo.png"
import { Button, Tooltip, Select } from 'antd';
import { getTasks, useGetTasks } from "@/services/service"
import { PlusCircleOutlined } from '@ant-design/icons';

type Props = {
    name: string
    setAddNewTaskVisible: (arg: number) => void
    index: number
}

const WeekdayHeader = (props: Props) => {
  const [hovered, setHovered] = useState(false)
  const showAddButton = hovered ? "visible" : "invisible"
  return (
        <div className= {`flex justify-center`} onMouseOver={() => {setHovered(true)}} onMouseOut={() => {setHovered(false)}}>
        <span className={`${showAddButton} origin-left`}>
          <Tooltip title="Add">
            <Button  onClick={() => props.setAddNewTaskVisible(props.index)} type="primary" shape="circle" icon={<PlusCircleOutlined />}  />
          </Tooltip>
          </span>    
            <p>{props.name}</p>
        </div>

  )
}

export default WeekdayHeader