import { useState } from "react"
import { Bars3Icon, XMarkIcon} from "@heroicons/react/24/solid"
import Logo from "@/assets/Logo.png"
import { Button, Tooltip, Select } from 'antd';
import { getTasks, useGetTasks } from "@/services/service"
import { PlusCircleOutlined } from '@ant-design/icons';
import { Input,  } from 'antd';
import { useAddTask } from "@/services/service"
type Props = {
    visible: boolean
    projects: Project[]
    orgId: string | undefined
    date: Date
    weekStart: Date
    addTask: any
}

const NewTask = (props: Props) => {
  const [taskTitle, setTaskTitle] = useState<string>("")
  const [taskDuration, setTaskDuration] = useState<number>(0)
  const [project, setProject] = useState<any|undefined>(undefined)

  if (!props.visible) {
    return (<p />)
  }
  
  const options:{
    value: string;
    label: string;
  }[] = props.projects.map ((p: Project) => {
    return {
      value: p.id,
      label: p.name
    }
  })

  return (
    <div className={`flex gap-8`}>
        <Input placeholder="Task title" onChange={ (e) => {setTaskTitle(e.target.value)}}/>
        <Select
        showSearch
        placeholder="Select a Project"
        optionFilterProp="children"
        onChange={(option) => {
            console.log(JSON.stringify(option))
          setProject({
            "projectId": option,
            "projectName": "?"
          })
          console.log(project)
        }
    }
        // onSearch={onSearch}
        options={options}
        />
        <Input placeholder="Duration" onChange={ (e) => {setTaskDuration(+e.target.value)}}/>
        <Tooltip title="Add">
        <Button 
        onClick={() => props.addTask.mutate({
            date: props.date, 
            title: taskTitle, 
            duration: taskDuration, 
            projectId: project.projectId, 
            projectName: project.projectName, 
            orgId: props.orgId, 
            weekStart: props.weekStart})} 
        type="primary" shape="circle" icon={<PlusCircleOutlined />}  />
        </Tooltip>
    </div>

  )
}

export default NewTask