import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
// import './App.css'
import Keycloak from 'keycloak-js';
import Navbar from "@/scenes/navbar"
import WeekSelector from '@/scenes/weekselector';

import { Table, Input, Pagination } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { PlusCircleOutlined } from '@ant-design/icons';
import { Button, Tooltip, Select } from 'antd';
import { getTasks, useGetTasks } from "@/services/service"
import TasksTable from "./tasksTable"

import {
  QueryClient,
  QueryClientProvider,
  useQuery,
  useMutation,
  useQueryClient
} from '@tanstack/react-query'

const queryClient = new QueryClient()

function formatDate(date = new Date()) {
  const year = date.toLocaleString('default', {year: 'numeric'});
  const month = date.toLocaleString('default', {
    month: '2-digit',
  });
  const day = date.toLocaleString('default', {day: '2-digit'});

  return [year, month, day].join('-');
}

type AppProps = {
    orgId: string | undefined
}

function TasksScreen(props: AppProps) {
  var wS: Date = new Date()
  wS.setDate(wS.getDate() - wS.getDay() + 1)
  wS.setHours(0,0,0,0)
  const weekEnd = new Date(wS)

  weekEnd.setDate(weekEnd.getDate() + 7)

  const [dateRange, setDateRange] = useState<DateRange>({from: wS, to: weekEnd})

  return <div className="app bg-gray-20 bg-gray">
    <WeekSelector setDateRange={setDateRange} dateRange={dateRange}/>
    <TasksTable orgId={props.orgId} dateRange={dateRange} />
  {/* <Example weekStart={weekStart} organization={props.organization}/> */}
</div>
}

export default TasksScreen
