type Props = {}
import { Button, Checkbox, Form, Input } from 'antd';
import { useNavigate } from 'react-router-dom';
  
  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  
  type FieldType = {
    login?: string;
    password?: string;
    remember?: string;
  };
  
const Auth = (props: Props) => {
  const navigate = useNavigate();
  const onFinish = (values: any) => {
    fetch('http://localhost:8080/login', {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({login: values.login, password: values.password})
        }).then(response => response.text())
        .then(data => {
            sessionStorage.setItem('token', data);
            const xxx = sessionStorage.getItem('token')
            console.log('sho ' + xxx)
            navigate('/')
        })
    
    
  };
  const onFinishRegister = (values: any) => {
    fetch('http://localhost:8080/register', {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({login: values.login, password: values.password, username: "TODO"})
        }).then(response => response.text())
        .then(data => {
            sessionStorage.setItem('token', data);
            const xxx = sessionStorage.getItem('token')
            console.log('sho ' + xxx)
            navigate('/')
        })
    
    
  };

  return (
    <div>
      <h1>Login</h1>
      <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      style={{ maxWidth: 600 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item<FieldType>
        label="Username"
        name="login"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item<FieldType>
        label="Password"
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
    <h1>Register</h1>
      <Form
      name="basic2"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      style={{ maxWidth: 600 }}
      initialValues={{ remember: true }}
      onFinish={onFinishRegister}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item<FieldType>
        label="Login"
        name="login"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item<FieldType>
        label="Password"
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  </div>
  )
}

export default Auth