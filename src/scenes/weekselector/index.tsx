import { useState } from "react"
import { Bars3Icon, XMarkIcon} from "@heroicons/react/24/solid"
import Logo from "@/assets/Logo.png"
import { Button, Tooltip, Select } from 'antd';
type Props = {
    setDateRange: any;
    dateRange: DateRange;
}


const WeekSelector = (props: Props) => {

  return (

        <div>
            <Button>Today</Button>
            <Button onClick={() => {
                props.dateRange.from.setDate(props.dateRange.from.getDate() - 7)
                props.dateRange.to.setDate(props.dateRange.to.getDate() - 7)
                props.setDateRange({from: new Date(props.dateRange.from), to: new Date(props.dateRange.to) })
                }}>Left</Button>
            <Button onClick={() => {
                props.dateRange.from.setDate(props.dateRange.from.getDate() + 7)
                props.dateRange.to.setDate(props.dateRange.to.getDate() + 7)
                props.setDateRange({from: new Date(props.dateRange.from), to: new Date(props.dateRange.to) })
                }}>Right</Button>
        </div>

  )
}

export default WeekSelector