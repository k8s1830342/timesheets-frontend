type Props = {}
import { Button, Checkbox, Form, Input } from 'antd';
import { useNavigate } from 'react-router-dom';
  
  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  
  type FieldType = {
    organization: string;
  };
  
const Organizations = (props: Props) => {
  const navigate = useNavigate();
  const onFinish = (values: any) => {
    fetch('http://localhost:8080/graphql', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + sessionStorage.getItem('token')
      },
      body: JSON.stringify({query: `mutation addOrg($orgName: String!) {
        addOrganization(name: $orgName) {
          id
        }
      }
      `,
      variables: {"orgName": values.organization}
    })
    }).then(response => response.json())
    .then(_ => {
        navigate('/')
    })
    
    
  }

  return (
    <Form
    name="basic"
    labelCol={{ span: 8 }}
    wrapperCol={{ span: 16 }}
    style={{ maxWidth: 600 }}
    initialValues={{ remember: true }}
    onFinish={onFinish}
    onFinishFailed={onFinishFailed}
    autoComplete="off"
  >
    <Form.Item<FieldType>
      label="Organization"
      name="organization"
      rules={[{ required: true, message: 'Please enter Organization name!' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
      <Button htmlType="submit">
        Submit
      </Button>
    </Form.Item>
  </Form>
  )
}

export default Organizations