type Props = {
    organization: string | undefined
}
import { Button, Checkbox, Form, Input } from 'antd';
import { useNavigate } from 'react-router-dom';
  
  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  
  type FieldType = {
    project: string;
  };
  
const Projects = (props: Props) => {
  const navigate = useNavigate();
  const onFinish = (values: any) => {
    fetch('http://localhost:8080/graphql', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + sessionStorage.getItem('token')
      },
      body: JSON.stringify({query: `mutation addProject($orgId: ID!, $projName: String!) {
        addProject(organizationId: $orgId, name: $projName) {
          id
        }
      }
      `,
      variables: {"projName": values.project, "orgId": props.organization}
    })
    }).then(response => response.json())
    .then(_ => {
        navigate('/')
    })
    
    
  }

  return (
    <Form
    name="basic"
    labelCol={{ span: 8 }}
    wrapperCol={{ span: 16 }}
    style={{ maxWidth: 600 }}
    initialValues={{ remember: true }}
    onFinish={onFinish}
    onFinishFailed={onFinishFailed}
    autoComplete="off"
  >
    <Form.Item<FieldType>
      label="Project"
      name="project"
      rules={[{ required: true, message: 'Please enter Project name!' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
      <Button htmlType="submit">
        Submit
      </Button>
    </Form.Item>
  </Form>
  )
}

export default Projects