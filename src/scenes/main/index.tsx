type Props = {
}
import { Button, Checkbox, Form, Input } from 'antd';
import { useNavigate } from 'react-router-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import TasksScreen from '@/scenes/tasksScreen'
import Auth from '@/scenes/auth'
import Organizations from '@/scenes/organization'
import Navbar from "@/scenes/navbar"
import { nieweim } from "@/services/service"

import {
    QueryClient,
    QueryClientProvider,
    useQuery,
    useMutation,
    useQueryClient
  } from '@tanstack/react-query'
  


import Projects from '@/scenes/projects'
import { useEffect, useState } from 'react';

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  
  type FieldType = {
    project: string;
  };
  
const Main = (props: Props) => {
    
    // const navigate = useNavigate();
    // const {data, isPending, isError} = useQuery(getOrganizations)

    
    // const firstOrg = organizations?

    return (
      <Routes>
        <Route path="/*" element={ <CheckAuth/>} />
        <Route path="/auth" element={<Auth />} />
      </Routes>
    )
}

const CheckAuth = (props: Props) => {
    
    const navi = useNavigate()
    const [orgs, setOrgs] = useState(undefined)
    const [selectedOrg, setSelectedOrg] = useState(undefined)
    useEffect(() => {
        
        const x = fetch('http://localhost:8080/graphql', {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + sessionStorage.getItem('token')
          },
          body: JSON.stringify({query: `query getOrganizations{
            getOrganizations {
              id
              name
            }
          }`})
        }).then((res) => {
            return res.json()
        }).then((data) => {
            setOrgs(data.data.getOrganizations)
            setSelectedOrg(data.data.getOrganizations[0].id)
        }).catch((_) => {
            navi('/auth')
        })
    }, [setOrgs, setSelectedOrg])

    // const { organizations } = nieweim()

    // var organization: string | undefined
    // if (organizations !== undefined && organizations.length > 0) {
    //     organization = organizations[0].id
    //     console.log("xd = " + organization)
    // }

    return (<div>
        <Navbar organization={selectedOrg} setOrganization={setOrgs} organizations={orgs} />
        <Routes>
            <Route path="/" element={ <TasksScreen orgId={selectedOrg}/>} />
            <Route path="/org" element={<Organizations />} />
            <Route path="/projects" element={<Projects organization={selectedOrg}/>} />
            <Route path="/auth" element={<Auth />} />
        </Routes>
        </div>

    )
}

export default Main