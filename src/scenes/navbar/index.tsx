import { useState } from "react"
import { Bars3Icon, XMarkIcon} from "@heroicons/react/24/solid"
import Logo from "@/assets/Logo.png"
import { Dropdown, Space, Button } from 'antd';
import type { MenuProps } from 'antd';

type Props = {
    organizations: {id:string, name: string}[] | undefined
    organization: string | undefined
    setOrganization: any
}


const Navbar = (props: Props) => {

    const items: MenuProps['items'] = props.organizations === undefined ? [] : props.organizations.map((o) => {
        return {
            key: o.id,
            label: (
                <Button onClick={() => props.setOrganization(o.id)}>{o.name}</Button>
              ),
        }
    })

  return (
    <div>XD XD XD XD XD XD
    <Dropdown trigger={['click']} menu={{ items }}>
    <a onClick={(e) => e.preventDefault()}>
      <Space>
        Hover me
      </Space>
    </a>
  </Dropdown>
  </div>
  )
}

export default Navbar