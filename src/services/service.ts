
import { useNavigate } from 'react-router-dom';

import {
    QueryClient,
    QueryClientProvider,
    useQuery,
    useMutation,
    useQueryClient,
    QueryFunction,
    keepPreviousData
  } from '@tanstack/react-query'

type OrganizationsResponse = {
    data: OrganizationsResponseData
}

type OrganizationsResponseData = {
    getOrganizations: Organization
}

type Organization = {
    id: string
    name: string
}

function formatDate(date = new Date()) {
    const year = date.toLocaleString('default', {year: 'numeric'});
    const month = date.toLocaleString('default', {
      month: '2-digit',
    });
    const day = date.toLocaleString('default', {day: '2-digit'});
  
    return [year, month, day].join('-');
  }

export function nieweim(): {isError: boolean, isPending: boolean, organizations: { id: string, name: string}[] | undefined} {
    
    const navigate = useNavigate();
    const {data, isPending, isError} = useQuery({
    
        queryKey: ['organizations'],
        queryFn: graphqlRequest(`query getOrganizations{
            getOrganizations {
              id
              name
            }
          }`, {})
        })
        if (data !== undefined && "error" in data) {
        console.log("data !== undefined && error in data")
        navigate('/auth')
        return {isError: true, isPending: isPending, organizations: undefined}
    }
    return {isError: isError, isPending: isPending, organizations: data?.data?.getOrganizations}
}

export function getTasks(orgId: string, from: Date, to: Date): {isError: boolean, isPending: boolean, data: any } {
    console.log("orgId + " + orgId)
    return req(`query getTasks($orgId: ID!, $from: Date!, $to: Date!) {
        tasks(organizationId: $orgId, from: $from, to: $to) {
          total
          tasks {
            id
            title
            date
            minutes
            project {
              id
              name
            }
          }
        }
      }`, {"orgId": orgId, "from": formatDate(from), "to": formatDate(to)}, ['tasks', orgId])
}

export function useGetTasks(orgId: string | undefined, from: Date, to: Date) {
    return useQuery({
        queryKey: ['tasks', from, orgId],
        queryFn: () => {
          if (orgId === undefined) {
            throw {}
          }
          return fetch('http://localhost:8080/graphql', {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + sessionStorage.getItem('token')
          },
          body: JSON.stringify({query: `query getTasks($orgId: ID, $from: Date!, $to: Date!) {
            tasks(organizationId: $orgId, from: $from, to: $to) {
              total
              tasks {
                id
                title
                date
                minutes
                project {
                  id
                  name
                }
              }
            }
          }`,
          variables: {"orgId": orgId, "from": formatDate(from), "to": formatDate(to)}
        })
        }).then(
            (res) => res.json(),
          ) },
        placeholderData: keepPreviousData
      })
}

export function useGetProjects(orgId: string | undefined) {
    return useQuery({
    
        queryKey: ['projects', orgId],
        queryFn: async () => {
          if (orgId === undefined) {
            throw {}
          }
          const res = await fetch('http://localhost:8080/graphql', {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Bearer " + sessionStorage.getItem('token')
            },
            body: JSON.stringify({
              query: `query getProjects($orgId: ID!) {
            getProjects(organizationId: $orgId) {
              total
              pageNo
              projects {
                id
                name
              }
            }
          } `,
              variables: { "orgId": orgId }
            })
          });
          return await res.json();}
      })
}

export function useAddTask() {
  const queryClient = useQueryClient()
  return useMutation({
    mutationFn: (newTask: any) =>
    fetch('http://localhost:8080/graphql', {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + sessionStorage.getItem('token')
    },
    body: JSON.stringify({query: `mutation addTask($title: String!, $projectId: ID!, $date: Date!, $duration: Int!) {
      addTask(title: $title, projectId: $projectId, date: $date, duration: $duration) {
        id
      }
    }`,
    variables: {"title": newTask.title, "projectId": newTask.projectId, "date": formatDate(newTask.date), "duration": newTask.duration}
  })
  }).then(
      (res) => res.json(),
    ),
    onSuccess: (data, context)  => {
      // setNewTaskDay(undefined)

      const current: any = queryClient.getQueryData(['tasks', context.weekStart, context.orgId])
      current.data.tasks.tasks.splice(0, 0, {
        "id": data.data.addTask.id,
        "title":context.title,
        "date":formatDate(context.date),
        "minutes": context.duration, // Conversion
        "project":{"id":context.projectId,"name":context.projectName}})
      queryClient.setQueryData(['tasks', context.weekStart, context.orgId], current)
    }
  })
}

function req(query: string, variables: any, queryKey: string[]) {
    const navigate = useNavigate();
    const {data, isPending, isError} = useQuery({
        queryKey: queryKey,
        queryFn: graphqlRequest(query, variables)
      })
    if (data !== undefined && "error" in data) {
        navigate('/auth')
        return {isError: true, isPending: isPending, data: undefined}
    }
    return { isError: isError, isPending: isPending, data: data }
}

function graphqlRequest(query: string, variables: any): QueryFunction<any, string[], never> | undefined {
    return () => {
        console.log("fetch")
        return fetch('http://localhost:8080/graphql', {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + sessionStorage.getItem('token')
          },
          body: JSON.stringify({query: query, variables: variables})
        }).then(
            (res) => {
                if (!res.ok) {
                    return {"error": res.status}
                }
                return res.json()
            },
          )
}}

// export const getOrganizations = 
