type DateRange = {
    from: Date
    to: Date
  }
  

  type Project = {
    id: string;
    name: string;
}