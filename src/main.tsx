import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter, Routes, Route } from "react-router-dom"
import App from './App.tsx'
import Auth from '@/scenes/auth'
import Main from '@/scenes/main'
import Organizations from '@/scenes/organization'
import {
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query'
import './index.css'
import Projects from './scenes/projects/index.tsx'

const queryClient = new QueryClient()
ReactDOM.createRoot(document.getElementById('root')!).render(
  <BrowserRouter>
      <QueryClientProvider client={queryClient}>
      <Main />
      </QueryClientProvider>
    </BrowserRouter>
  // <React.StrictMode>
   
  // </React.StrictMode>,
)
